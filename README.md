Desafio Cientista de Dados B
================

> Desafio prático para Cientista de Dados do tipo B - **Nexer Labs**

Introdução
----------

Como um **Cientista de Dados B** (Building) você deve demonstrar conhecimentos de estatística e matemática combinados com um bom *background* de desenvolvimento de *software*.

Neste desafio testaremos seus *skills* de desenvolvimento e sua capacidade analítica de uma forma um tanto quanto inusitada. Ao final, você perceberá que extrair dados da internet e gerar *insights* analíticos é uma tarefa bem divertida!

Divirta-se!

Instruções
----------

Você deverá realizar todas as tarefas a seguir, atentando-se para as considerações da seção **Requisitos**. Leia todas as instruções e requisitos antes de começar a trabalhar, e boa sorte!

-   Crie um `fork` do repositório [nexer-dsb-challenge](https://bitbucket.org/nexerlabs/nexer-dsb-challenge). A ideia é que você modifique o repositório e faça um `pull` *request* ao final.
-   Abra o arquivo `./data/pbe_table_2016.html` em seu *browser*. Você verá o resultado do [PBE 2016](http://www.inmetro.gov.br/consumidor/tabelas_pbe_veicular.asp) (Programa de Etiquetagem Veicular), que avaliou o consumo de todos os modelos brasileiros de carro de 2016. Estamos interessados na última tabela da página: ![PBE 2016](./images/pbe_2016.png)
-   Crie um *Jupyter notebook* com nome `notebook.ipynb` na raiz do repositório. Leia o **Requisito 3** para mais detalhes.
-   Extraia a tabela do arquivo `HTML` usando as bibliotecas em `Python` de sua preferência (Ex: `BeautifulSoup`, `scrapy`, `robobrowser`).
-   Crie um *data frame* `pandas` com os dados extraídos e imprima as 10 primeiras linhas. A saída deve ser semelhante a:

| category     | brand | model    | version                     |  displacement|  valves| transmission |  gears| air\_conditioner | steering | fuel\_type |  nmhc\_emission|  co\_emission|  nox\_emission|  co2\_emission\_eta|  co2\_emission\_gas|  consumption\_eta\_city|  consumption\_eta\_highway|  consumption\_gas\_city|  consumption\_gas\_highway| relative\_label | absolute\_label |
|:-------------|:------|:---------|:----------------------------|-------------:|-------:|:-------------|------:|:-----------------|:---------|:-----------|---------------:|-------------:|--------------:|-------------------:|-------------------:|-----------------------:|--------------------------:|-----------------------:|--------------------------:|:----------------|:----------------|
| sub compacto | chery | qq       | 1.0                         |           1.0|      12| m            |      5| yes              | h        | g          |           0.014|         0.132|          0.105|                  NA|                 105|                      NA|                         NA|                    11.8|                       13.9| c               | c               |
| sub compacto | chery | face     | 1.3 flex                    |           1.3|      16| m            |      5| yes              | h        | f          |           0.023|         0.513|          0.045|                   0|                 120|                     7.0|                        7.4|                    10.3|                       12.6| e               | e               |
| sub compacto | fiat  | uno      | mille fire economy 2 portas |           1.0|       8| m            |      5| no               | m        | f          |           0.025|         0.345|          0.044|                   0|                  95|                     8.9|                       10.7|                    12.7|                       15.6| a               | a               |
| sub compacto | fiat  | uno      | mille fire economy 4 portas |           1.0|       8| m            |      5| no               | m        | f          |           0.025|         0.345|          0.044|                   0|                  95|                     8.9|                       10.7|                    12.7|                       15.6| a               | a               |
| sub compacto | fiat  | uno      | mille way economy 2 portas  |           1.0|       8| m            |      5| no               | m        | f          |           0.025|         0.345|          0.044|                   0|                 102|                     8.6|                        9.7|                    12.4|                       13.8| b               | b               |
| sub compacto | fiat  | uno      | mille way economy 4 portas  |           1.0|       8| m            |      5| no               | m        | f          |           0.025|         0.345|          0.044|                   0|                 102|                     8.6|                        9.7|                    12.4|                       13.8| b               | b               |
| sub compacto | fiat  | novo uno | economy evo 2 portas        |           1.4|       8| m            |      5| no               | m        | f          |           0.024|         0.355|          0.027|                   0|                  97|                     8.7|                       10.4|                    12.5|                       15.2| a               | a               |
| sub compacto | fiat  | novo uno | economy evo 4 portas        |           1.4|       8| m            |      5| no               | m        | f          |           0.024|         0.355|          0.027|                   0|                  97|                     8.7|                       10.4|                    12.5|                       15.2| a               | a               |
| sub compacto | fiat  | novo uno | vivace evo 2 portas         |           1.0|       8| m            |      5| no               | m        | f          |           0.029|         0.347|          0.046|                   0|                 100|                     8.3|                        9.4|                    12.3|                       14.5| b               | b               |
| sub compacto | fiat  | novo uno | vivace evo 4 portas         |           1.0|       8| m            |      5| no               | m        | f          |           0.029|         0.347|          0.046|                   0|                 100|                     8.3|                        9.4|                    12.3|                       14.5| b               | b               |

-   Seu *data frame* deve atender todos os princípios de **Tidy Data**. Leia o artigo [Tidy Data](http://vita.had.co.nz/papers/tidy-data.html) (Wickham 2014) para saber mais.
-   Exporte o *data frame* para um arquivo `CSV` na pasta `data`. O arquivo `CSV` deve ter o mesmo nome do arquivo `HTML` e deve manter o mesmo formato (mesmo nome, tipo e formato das colunas) do arquivo de exemplo `sample.csv`. Valores faltantes devem ser preenchidos com `NA`.
-   **Manipulação dos dados:** você deverá utilizar a biblioteca `pandas` para manipular os dados do **PBE 2016** e responder às seguintes perguntas:
    -   Qual carro (Marca, Modelo e Versão) você me recomendaria se eu estiver interessado em comprar um modelo compacto, flex, com motor (*displacement*) 1.6, e que seja muito econômico em cidade?
    -   Você já deve ter escutado que para o álcool ser mais vantajoso do que a gasolina, o preço do litro tem que custar até 70% do litro da gasolina. Leia esta [matéria](http://www.meubolsoemdia.com.br/ferramentas/calculadora-alcool-Gasolina) para saber mais. Essa proporção de 70% é verdadeira? Confira usando os dados de consumo do **PBE 2016**. Você deverá calcular a **média** e o **desvio padrão** dessa proporção para carros compactos com motor flex 1.6, tanto para cidade quanto para estrada.
-   **Visualização dos dados:** você deverá utilizar a biblioteca `matplotlib` para visualizar os dados do **PBE 2016** e extrair os seguintes insights:
    -   O motor do carro interfere no consumo de combustível? Considerando apenas veículos compactos e consumo de gasolina em cidade, plote em um mesmo gráfico um *boxplot* de consumo para cada categoria de `displacement` (considere apenas 1.0, 1.4, 1.5 e 1.6), para visualizar a tendência central e a dispersão dos valores. Tire suas conclusões. Abaixo segue um exemplo de gráfico: ![PBE 2016](./images/displacement_boxplot.png)
    -   E quanto ao famoso ar condicionado, aumenta mesmo o consumo em cidade? Faça um gráfico semelhante ao anterior, um *boxplot* para consumo com ar condicionado e outro para consumo sem ar condicionado. Considere apenas carros compactos da Fiat, com câmbio manual e motor 1.0.
-   Salve seu *notebook* e faça um `pull` request!

Requisitos
----------

1.  Todos os códigos devem ser feitos em `Python`.
2.  Não pode haver qualquer processo manual na extração e manipulação dos dados. Tudo deve ser feito via código, desde a leitura do arquivo `HTML` até a exportação dos dados para o formato exigido.
3.  Você deverá organizar seu código em um [Jupyter](http://jupyter.org/) *notebook*, explicando passo-a-passo todas as suas decisões. A ideia é que você intercale células em `Markdown` com células em `Python` para tornar seu *notebook* o mais explicativo possível. Boa comunicação é um *skill* essencial de um cientista de dados! A organização do *notebook* fica a seu critério. Certifique-se de executar e testar todas as células de código do seu *notebook*, pois tudo deve ser reprodutível. ![PBE 2016](./images/jupyter_notebook.png)
4.  Você terá **48 horas** contando a partir do envio do teste para fazer o `pull` request.
5.  Use ao máximo sua criatividade! Você deve cumprir com tudo que foi solicitado, mas não precisa se restringir a isso.

------------------------------------------------------------------------

### References

Wickham, Hadley. 2014. “Tidy Data.” *The Journal of Statistical Software* 59 (10). <http://www.jstatsoft.org/v59/i10/>.
